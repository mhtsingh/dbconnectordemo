from django.apps import AppConfig


class ConnectorapiConfig(AppConfig):
    name = 'connectorAPI'
