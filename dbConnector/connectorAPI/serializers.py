from rest_framework import serializers


class RequestSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    host = serializers.CharField()
    database = serializers.CharField()
    table = serializers.CharField(required=False)
    batch = serializers.IntegerField(required=False, default=500)
