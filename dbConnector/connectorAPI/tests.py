from django.conf import settings
from django.test import TestCase
from sqlalchemy import create_engine

from . import operations


class DbConnectorTestCase(TestCase):

    def setUp(self):
        self.engine = None
        self.required_data = {
            "username": "root",
            "password": "Bliadmin#123",
            "host": "127.0.0.1",
            "database": "connector_db"
        }
        self.username = self.required_data.get("username")
        self.password = self.required_data.get("password")
        self.host = self.required_data.get("host")
        self.database = self.required_data.get("database")
        self.url = f"mysql+pymysql://{self.username}:{self.password}@"
        self.url += f"{self.host}/{self.database}"

    def test_connection(self):
        self.engine = create_engine(self.url).connect()
        query = f"SELECT @@version"
        result, = self.engine.execute(query)
        response, = result
        self.assertNotEqual(response, None)
        self.engine.close()

    def test_get_table_list(self):
        self.engine = create_engine(self.url).connect()
        response = []
        db_connection = operations.MySQLConnector(
            **self.required_data)
        mock_response = db_connection.get_table_list()
        db_connection.close_connection()

        query = f"""SELECT table_name FROM information_schema.tables 
                where table_schema='{self.database}'"""
        result = self.engine.execute(query)
        for table, in result:
            response.append(table)
        self.assertEqual(response, mock_response)
        self.engine.close()

    def test_get_sample_records(self):
        table = settings.TABLE_NAME
        self.engine = create_engine(self.url).connect()
        db_connection = operations.MySQLConnector(
            **self.required_data)
        mock_response = db_connection.get_sample_records(table)
        db_connection.close_connection()

        query = f"SELECT * FROM {table} LIMIT 5"
        result = self.engine.execute(query)
        response = [{key: value for (key, value) in row.items()}
                    for row in result]
        self.assertEqual(response, mock_response)
        self.engine.close()

    def test_get_sample_batch_records(self):
        table = settings.TABLE_NAME
        batch = settings.BATCH_SIZE
        self.engine = create_engine(self.url).connect()
        db_connection = operations.MySQLConnector(
            **self.required_data)
        mock_response = db_connection.get_sample_batch_record(table,
                                                              batch)
        db_connection.close_connection()

        record, = self.engine.execute(
            f"SELECT count('id') FROM {table}")
        count, = record
        table_id = count
        counter = 1
        final_result = []
        if table_id - batch <= 0:
            return
        while table_id - batch >= 0:
            query = f"""SELECT * FROM {table} WHERE 
                    {table}.id <= {table_id} ORDER BY 
                    {table}.id DESC LIMIT {batch}"""
            result = self.engine.execute(query)
            batch_result = [{key: value for (key, value) in row.items()}
                            for row in result]
            final_result.append({f"batch_{counter}": batch_result})
            table_id = table_id - batch
            counter += 1
        self.assertEqual(final_result, mock_response)
        self.engine.close()
