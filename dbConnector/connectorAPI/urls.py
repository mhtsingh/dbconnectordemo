from django.urls import path

from connectorAPI.views import (
    TableList, SampleRecord, BatchRecord)

urlpatterns = [
    path('api/table_list', TableList.as_view()),
    path('api/sample_record', SampleRecord.as_view()),
    path('api/batch_record', BatchRecord.as_view())
]
