import sqlalchemy
from sqlalchemy import create_engine


class MySQLConnector:

    def __init__(self, **kwargs):
        """
        create the url using param

        :kwargs
        """
        self.url = None
        self.engine = None

        username = kwargs.get('username')
        password = kwargs.get('password')
        host = kwargs.get('host')
        self.db = kwargs.get('database')
        self.url = f"mysql+pymysql://"
        self.url += f"{username}:{password}@{host}/{self.db}"
        self.connect_db()

    def close_connection(self):
        """Close db connection"""

        if self.engine:
            self.engine.close()
            self.engine = None

    def connect_db(self):
        """
        connect to db using variable url

        :return: self.engine
        """

        try:
            if not self.engine:
                self.engine = create_engine(self.url).connect()
        except Exception as e:
            print(e)
            raise Exception(e)
        return self.engine

    def get_table_list(self):
        """
        get all the table names in the connected db

        :return: table_list
        """
        response = []

        query = sqlalchemy.text(f"""SELECT table_name FROM 
                information_schema.tables where 
                table_schema='{self.db}'""")
        result = self.connect_db().execute(query)
        for table, in result:
            response.append(table)
        self.close_connection()
        return response

    def get_table_description(self, table):
        """
        return table descriptions as per the given table

        :param table
        :return: response_data
        """

        response = []

        query = sqlalchemy.text(f"desc {table}")
        result = self.connect_db().execute(query)
        for row in result:
            response.append(row)
        return response

    def get_record_count(self, table):
        """
        return number of records present in a table

        :return count
        """

        query = sqlalchemy.text(f"select count(id) from {table}")
        result, = self.connect_db().execute(query)
        response, = result
        return response

    def get_sample_records(self, table):
        """
        return inner join result of two tables given in required_data

        :return: response
        """

        query = sqlalchemy.text(f"SELECT * FROM {table} LIMIT 5")
        result = self.engine.execute(query)
        response = [{key: value for (key, value) in row.items()}
                    for row in result]
        self.close_connection()
        return response

    def get_sample_batch_record(self, table, batch=500):
        """
        return all records in batch in descending order

        :param table
        :param batch
        :return: response
        """

        count = self.get_record_count(table)
        table_id = count
        counter = 1
        response = []

        if table_id - batch <= 0:
            batch = count

        while table_id - batch >= 0:
            query = sqlalchemy.text(f"""SELECT * FROM {table} WHERE 
                    {table}.id <= {table_id} ORDER BY 
                    {table}.id DESC LIMIT {batch}""")
            result = self.engine.execute(query)
            batch_result = [{key: value for (key, value) in row.items()}
                            for row in result]
            response.append({f"batch_{counter}": batch_result})
            table_id = table_id - batch
            counter += 1
        self.close_connection()
        return response

    def get_sample_batch_record_asc(self, table, batch=50):
        """
        return all records in batch in ascending order

        :param batch
        :param table
        :return: response
        """

        count = self.get_record_count(table)
        offset = count // batch
        table_id = 0
        counter = 1
        response = []

        while table_id + batch <= offset * batch:
            query = sqlalchemy.text(f"""SELECT * FROM {table} 
                    WHERE {table}.id > {table_id} 
                    ORDER BY {table}.id ASC LIMIT {batch}""")
            result = self.engine.execute(query)
            batch_result = [{key: value for (key, value) in row.items()}
                            for row in result]
            response.append({f"batch_{counter}": batch_result})
            table_id += 10
            counter += 1
        self.close_connection()
        return response

# db = MySQLConnector(
#     **{
#         "username": "root",
#         "password": "Bliadmin#123",
#         "host": "127.0.0.1",
#         "database": "connector_db"
#     }
# )
# all_table = db.get_table_list()
# db.get_table_description(all_table[0])
# db.get_record_count(all_table[0])
# db.get_sample_records(all_table[0])
# db.get_sample_batch_record_asc(all_table[0], 10)
# db.get_sample_batch_record(all_table[0])
# cProfile.run('db.get_sample_batch_record_desc(10)')
