from rest_framework.response import Response
from rest_framework.views import APIView

from . import operations
from .serializers import RequestSerializer


class TableList(APIView):
    @staticmethod
    def post(request):
        """
        Return list of tables in the form of API response

        :request.data
        :return:
        {
            "table_list": [String, String]
        }
        """

        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            try:
                db_connection = operations.MySQLConnector(
                    **serializer.data)
                table_list = db_connection.get_table_list()
                return Response({"table_list": table_list})
            except Exception as e:
                return Response({"error": e})
        else:
            return Response({"error": serializer.errors})


class SampleRecord(APIView):
    @staticmethod
    def post(request):
        """
        Return 5 sample records and table descriptions
        in the form of API response

        :request.data
        :return:
        {
            "table_description": [dict]
            "record_list": [dict]
        }
        """

        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            try:
                db_connection = operations.MySQLConnector(
                    **serializer.data)
                table_list = db_connection.get_table_description(
                    serializer.data.get('table'))
                sample_record = db_connection.get_sample_records(
                    serializer.data.get('table'))
                return Response(
                    {
                        "table_description": table_list,
                        "record_list": sample_record
                    }
                )
            except Exception as e:
                return Response({"error": e})
        else:
            return Response({"error": serializer.errors})


class BatchRecord(APIView):
    @staticmethod
    def post(request):
        """
        Return sample batch records in the form of API response

        :request.data
        :return:
        {
            "batch_record": [dict]
        }
        """

        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            try:
                db_connection = operations.MySQLConnector(
                    **serializer.data)
                record_list = db_connection.get_sample_batch_record(
                    serializer.data.get('table'),
                    serializer.data.get('batch'))
                return Response({"batch_record": record_list})
            except Exception as e:
                return Response({"error": e})
        else:
            return Response({"error": serializer.errors})
